﻿#include <iostream>
#include <cassert>
#include "StackTemplate.h"
using namespace std;

int main()
{
    int n;
    int i = 0;
    Stack <int> intStek;
    cout << "Enter the size of your stack: ";
    cin >> n;
    cout << "Enter " << n << " number(s): ";
    while (i != n)
    {
        int a;
        cin >> a;
        intStek.push(a);
        i++;
    }
    cout << "Initial stack: "; intStek.printStack();
    cout << "\nTop element: " << intStek.showTop();
    cout << "\nPush element..."; intStek.push(20);
    cout << "\nUpdated stack: "; intStek.printStack();
    cout << "\nPop element... "; intStek.pop();
    cout << "\nPop element... "; intStek.pop();
    cout << "\nTop element: " << intStek.showTop();
    cout << "\nPop element: "; intStek.pop();
    cout << "\nFinal stack: "; intStek.printStack();
    cout << endl;

    Stack <string> strStek(3);
    strStek.push("Hi");
    strStek.push("everybody");
    strStek.push("there!");
    cout << "\nInitial stack: "; strStek.printStack();
    cout << "\nTop element: " << strStek.showTop();
    cout << "\nPop element..."; strStek.pop();
    cout << "\nFinal stack: "; strStek.printStack();
    cout << endl;

    return 0;

}

