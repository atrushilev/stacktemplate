#include <iostream>
#include <cassert>
using namespace std;
template <typename T>
class Stack
{
private:
    int m_length = 0;
    T* m_data = nullptr;
    T* top_ = nullptr;
public:
    Stack() : m_length(0), m_data(nullptr)
    {}
    Stack(int length)
    {
        assert(length >= 0);
        m_data = new T[length];
        top_ = m_data;
        m_length = length;
    }

    ~Stack()
    {
        delete[] m_data;
    }

    void push(T);
    T pop();
    void printStack();
    T showTop();

};

template<class T>
void Stack<T> :: push(T value)
{
    int used = top_ - m_data;
    if (used >= m_length) {
        T* data = new T[used + 1];
        memcpy(data, m_data, used * sizeof(int));
        top_ = data + used;
        m_length = used + 1;
        delete[] m_data;
        m_data = data;
    }
    *top_ = value;
    ++top_;
}

template<class T>
T Stack<T> ::pop()
{
    assert(m_length > 0);
    T top = m_data[m_length - 1];
    --m_length;
    return top;
}

template<class T>
void Stack<T> ::printStack()
{
    for (int i = 0; i < m_length; i++)
        cout << m_data[i] << ' ';
}

template<class T>
T Stack<T> ::showTop()
{
    T top = m_data[m_length - 1];
    return top;
}

